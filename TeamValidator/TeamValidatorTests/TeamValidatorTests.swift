//
//  TeamValidatorTests.swift
//  TeamValidatorTests
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//
//

import XCTest
@testable import TeamValidator

class TeamValidatorTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMixedSample() {
        let input =
            [
                ["Dreamers", "Driver", "Analytical", "Amiable"],
                ["Blueberries", "Driver", "Analytical", "Amiable", "Expressive", "Driver", "Analytical", "Amiable", "Expressive"],
                ["Tramps", "Analytical", "Amiable", "Expressive"],
                ["Butterflies", "Driver", "Analytical", "Amiable", "Analytical", "Amiable", "Expressive"],
                ["Coffee Lovers", "Driver", "Analytical", "Expressive", "Driver", "Analytical", "Expressive"],
                ["Dolphins", "Driver", "Analytical", "Amiable", "Expressive", "Analytical", "Amiable"],
                ["Dream Team", "Amiable", "Expressive", "Amiable", "Expressive"],
                ["Gazelles", "Driver", "Amiable", "Expressive"],
                ["The Fantastic Four", "Driver", "Analytical", "Amiable", "Expressive"],
                ["Unicorns", "Analytical", "Amiable", "Expressive"]
            ]
        let expectedOutput = ["Dreamers", "Tramps", "Coffee Lovers", "Dream Team", "Gazelles", "Unicorns"]
        XCTAssertEqual(findUnbalancedTeams(input), expectedOutput)
    }
    
    func testAllFour() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["The Fantastic Four", "Driver", "Analytical", "Amiable", "Expressive"]
            ]),
            []
        )
    }

    func testAllFourInDifferentOrder() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Other Fantastic Four", "Amiable", "Expressive", "Driver", "Analytical"]
            ]),
            []
        )
    }

    func testDriverMissing() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Tramps", "Amiable", "Expressive", "Analytical", "Amiable", "Expressive", "Analytical"]
            ]),
            ["Tramps"]
        )
    }

    func testAnalyticalMissing() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Miracle Workers", "Driver", "Amiable", "Expressive"]
            ]),
            ["Miracle Workers"]
        )
    }

    func testAmiableMissing() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Men In Black", "Driver", "Analytical", "Expressive"]
            ]),
            ["Men In Black"]
        )
    }

    func testExpressiveMissing() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Workaholics", "Driver", "Analytical", "Amiable"]
            ]),
            ["Workaholics"]
        )
    }

    func testEmptyTeam() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Ghosts"]
            ]),
            ["Ghosts"]
        )
    }

    func testNoTeam() {
        XCTAssertEqual(
            findUnbalancedTeams([
            ]),
            []
        )
    }

    func testBalancedAndUnbalanced() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Balanced", "Driver", "Analytical", "Amiable", "Expressive", "Driver", "Analytical", "Amiable", "Expressive"],
                ["Unbalanced", "Driver", "Analytical", "Amiable", "Driver", "Analytical", "Amiable"]
            ]),
            ["Unbalanced"]
        )
    }

    func testUnbalancedAndBalanced() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Unbalanced", "Driver", "Analytical", "Amiable", "Driver", "Analytical", "Amiable"],
                ["Balanced", "Driver", "Analytical", "Amiable", "Expressive", "Driver", "Analytical", "Amiable", "Expressive"]
            ]),
            ["Unbalanced"]
        )
    }

    func testTeamnameEqualsMissingSocialStyle() {
        XCTAssertEqual(
            findUnbalancedTeams([
                ["Driver", "Analytical", "Amiable", "Expressive"]
            ]),
            ["Driver"]
        )
    }


}
