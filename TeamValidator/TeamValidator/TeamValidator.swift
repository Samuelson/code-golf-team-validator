//
//  TeamValidator.swift
//  TeamValidator
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import Foundation

/**
 Function which validates an array of team compositions and returns an array with the names of unbalanced teams.
 A balanced team is a team which has at least one team member for each of the following social styles:
 "Analytical", "Driver", "Amiable", "Expressive".
 
 - parameter teamCompositions:
    Input is an Array of team compositions. Each team composition is an array of Strings and contains the name of team
    followed by the social styles of the team members.
 
 - returns: An array with the names of all unbalanced teams.
 
 There is no need to deal with input error situations in the implementation, instead
 you can assume that the input is always formally correct, that is
 
 - Each team composition contains at least one element with the team name
 - All other elements in a team composition are exactly on of
   "Analytical", "Driver", "Amiable" or "Expressive"
*/


func findUnbalancedTeams(_ teamCompositions: [[String]]) -> [String] {
    teamCompositions.filter{!Set(["Analytical", "Driver", "Amiable", "Expressive"]).isSubset(of: $0.dropFirst())}.map{$0[0]}
}
